from django.db import models

class OauthUser(models.Model):
    name = models.CharField(max_length=20)
    email = models.EmailField(unique=True)
    username = models.CharField(max_length=20)
    password = models.CharField(max_length=20)
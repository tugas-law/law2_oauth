from django.urls import re_path
from .views import index, verify, auth

urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path(r'^auth/$', auth, name='auth'),
    re_path(r'^verify/$', verify, name='verify'),
]

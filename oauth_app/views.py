import jwt
import os
import urllib.parse

from django.shortcuts import render, redirect
from oauth_app.models import OauthUser
from django.http import JsonResponse

jwt_secret = "LAWTugas2_RdPradiptaGitayaS"

# Fungsi untuk Index (Menampilkan halaman awal)
def index(request):
    context = {}
    return render(request, 'index.html', context)

# Fungsi untuk melakukan autentikasi
def auth(request):
    if request.method == 'GET':
        data = {'target': request.GET['target']}
        return render(request, 'auth.html', data)
    elif request.method == 'POST':
        try:
            user = OauthUser.objects.get(username=request.POST['username'], password=request.POST['password'])
            jwt_token = jwt.encode({'name': user.name, 'email': user.email}, jwt_secret, algorithm='HS256')
            return redirect(request.POST['target'] + '?token=' + urllib.parse.quote(jwt_token, safe=''))
        except NameError:
            return JsonResponse({'status': 'Pengguna Tidak Ditemukan!'})

# Fungsi untuk melakukan verifikasi yang kemudian akan digunakan untuk service yang lain
def verify(request):
    if request.method == 'GET':
        try:
            jwt.decode(request.GET['jwt_token'], jwt_secret, algorithms=['HS256'])
            return JsonResponse({'status': 'Autentikasi Berhasil!'})
        except jwt.InvalidSignatureError:
            return JsonResponse({'status': 'Autentikasi Gagal!'})
